(in-package elf-hooks)

;;; Grab size_t for mprotect.
(include "stddef.h")
(include "unistd.h")
(include "sys/mman.h")

;;; For mprotect
(ctype size-t "size_t")

;;; For sysconf
(constant (sc-pagesize "_SC_PAGESIZE"))
(constant (prot-none   "PROT_NONE"))
(constant (prot-read   "PROT_READ"))
(constant (prot-write  "PROT_WRITE"))
(constant (prot-exec   "PROT_EXEC"))
