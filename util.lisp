(in-package elf-hooks)

(defcfun mprotect
    :int
  (addr :pointer)
  (len  size-t)
  (prot :int))

(defcfun sysconf
    :long
  (name :int))

(defun get-page (address)
  "Return the page that ADDRESS is located in."
  (bind ((page-size (sysconf sc-pagesize))
         (page-mask (lognot (1- page-size))))
    (logand address page-mask)))

(defun %modify-page-permissions (address &key read write execute)
  "Modify the page permissions of the page that ADDRESS is located in."
  (bind ((addr (pointer-address address))
         (page-addr (get-page addr))
         (permissions-list (list (if read
                                     prot-read
                                     0)
                                 (if write
                                     prot-write
                                     0)
                                 (if execute
                                     prot-exec
                                     0)))
         (permissions (reduce #'logior permissions-list)))
    (mprotect (make-pointer page-addr) (- addr page-addr) permissions)))

(defun calculate-offset-to-callback (base-addr callback-pointer)
  "Return the offset from BASE-ADDR to CALLBACK-POINTER."
  (let ((base-addr-num (pointer-address base-addr))
        (callback-num  (pointer-address callback-pointer)))
    (if (< base-addr-num callback-num)
        (- callback-num base-addr-num)
        (+ (- base-addr-num) callback-num))))

(defun get-shared-library-address (library)
  "Return the base address of LIBRARY.
Parses /proc/self/maps to find it."
  (with-open-file (stream "/proc/self/maps")
    ;; Loop through file to find the library's line.
    (bind ((base-address-line (do ((line "" (read-line stream nil)))
                                  ((equal line nil))
                                (when (search library line)
                                  (return line))))
           (address-string (subseq base-address-line 0 (position #\- base-address-line))))
      ;; parse the address string and  create the pointer.
      (make-pointer (parse-integer address-string :radix 16)))))
