(in-package elf-hooks)

(defun %hook-foreign-function (library-path function-name replacement-function)
  "Hooks FUNCTION-NAME in LIBRARY-PATH to call REPLACEMENT-FUNCTION instead.
REPLACEMENT-FUNCTION needs to be defined by a call to cffi:defcallback and
should be a symbol. LIBRARY-PATH should be the absolute path to the shared
object which is having a function hooked. The function will be hooked internally
in the shared object indicated by LIBRARY-PATH."
  (with-open-file (stream library-path :direction :input :element-type '(unsigned-byte 8))
    (bind ((binary-types:*endian* :little-endian)
           (elf            (initialize-elf-with-stream stream))
           (symbol-index   (find-in-dynsym elf stream function-name)))
      (when symbol-index
        (bind ((plt-entry      (find-plt-entry-by-index elf stream symbol-index))
               (base-address   (get-shared-library-address library-path))
               (offset-address (inc-pointer base-address (slot-value plt-entry 'r-offset)))
               (previous-entry (mem-aref offset-address :pointer)))
          (setf (mem-aref offset-address :pointer) (get-callback replacement-function))
          previous-entry)))))

(defun %hook-foreign-dynsymbol (library-path symbol-name replacement-address)
  "Hooks SYMBOL-NAME in LIBRARY-PATH to be defined as REPLACEMENT-ADDRESS such that,
when runtime resolution is performed on the symbol's name, REPLACEMENT-ADDRESS is returned
instead. Returns the previous offset that was contained in the symbol entry. SYMBOL-NAME
will be hooked externally."
  (with-open-file (stream library-path :direction :input :element-type '(unsigned-byte 8))
    (bind ((binary-types:*endian* :little-endian)
           (elf            (initialize-elf-with-stream stream))
           (symbol-index   (find-in-dynsym elf stream symbol-name)))
      (when symbol-index
        (bind ((base-address   (get-shared-library-address library-path))
               (symbol-entry-offset (+ (slot-value (dynsym elf) 'sh-offset)
                                       (* (sizeof 'symbol-table-entry) symbol-index)))
               (symbol-entry-address (inc-pointer base-address symbol-entry-offset))
               (offset-address (inc-pointer symbol-entry-address
                                            (slot-offset 'symbol-table-entry 'st-value)))
               (replacement-offset (calculate-offset-to-callback base-address
                                                                 (get-callback replacement-address)))
               (previous-entry (mem-aref offset-address :pointer)))
          (%modify-page-permissions offset-address :read t :write t :execute t)
          (setf (mem-aref offset-address :int64) replacement-offset)
          (%modify-page-permissions offset-address :read t :execute t)
          previous-entry)))))
