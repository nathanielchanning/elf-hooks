(defpackage elf-hooks
  (:use cl binary-elf cffi bind binary-types)
  (:export %hook-foreign-function
           %hook-foreign-dynsym))
