(in-package cl-user)

(defpackage elf-hooks-asd
  (:use cl asdf))

(in-package elf-hooks-asd)

(defsystem elf-hooks
  :description "Hooks for elf files."
  :author "Nathaniel Channing <nathaniel@desktopxpressions.com>"
  :license "BSD 3-clause"
  :defsystem-depends-on ("cffi-grovel")
  :depends-on ("binary-types" "metabang-bind" "cffi")
  :serial t
  :components
  ((:module "binary-elf"
       :serial t
       :components
       ((:file "package")
        (:file "definitions")
        (:file "functions")))
   (:file "package")
   (:cffi-grovel-file "grovel")
   (:file "util")
   (:file "elf-hooks")))
