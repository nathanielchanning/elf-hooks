(in-package binary-elf)

(defun read-header (stream)
  "Returns an ELF HEADER read from STREAM."
  (file-position stream 0)
  (read-binary 'header stream))

(defun read-section-header-table (elf-object stream)
  "Read in the section header table from STREAM.
Returns an array of section-header entries."
  (with-slots (e-shoff e-shnum)
      (header elf-object)
    (file-position stream e-shoff)
    ;; Store the entries in an array.
    (let ((section-header-table (make-array e-shnum)))
      (dotimes (i e-shnum section-header-table)
        (let ((entry (read-binary 'section-header stream)))
          (setf (aref section-header-table i) entry))))))

(defun read-section-table-string (elf-object stream index)
  "Read the section table string located at INDEX."
  (bind (((:slots header section-table) elf-object)
         (string-table-index (slot-value header 'e-shstrndx))
         (strtab-section     (aref section-table string-table-index))
         (sh-offset          (slot-value strtab-section 'sh-offset)))
      (file-position stream (+ sh-offset index))
      (read-binary-string stream :terminators '(0))))

(defun find-in-section-table (elf-object stream search-string)
  "Find the section table entry which corresponds with search-string."
  (with-slots (section-table header)
      elf-object
         ;; bindings
    (do* ((i 0 (1+ i))
          (section       (aref section-table i)
                         (aref section-table i))
          (string-offset (slot-value section 'sh-name)
                         (slot-value section 'sh-name))
          (str           (read-section-table-string elf-object
                                                    stream
                                                    string-offset)
                         (read-section-table-string elf-object
                                                    stream
                                                    string-offset)))
         ;; end condition
         ((= i (length section-table)))
      (when (string= str search-string)
        (return section)))))

(defun read-string-table-string (elf-object stream index)
  "Read the string table string located at INDEX."
  (bind (((:accessors header section-table dynsym)
          elf-object)
         (link                 (slot-value dynsym 'sh-link))
         (string-table-section (aref section-table link))
         (sh-offset            (slot-value string-table-section 'sh-offset)))
    (file-position stream (+ sh-offset index))
    (read-binary-string stream :terminators '(0))))

(defun read-dynsym-section (elf-object stream)
  "Read the dynsym section"
  (find-in-section-table elf-object stream ".dynsym"))

(defun find-in-dynsym (elf-object stream search-symbol)
  "Find SEARCH-SYMBOL in the dynsym section.
Returns the index SEARCH-SYMBOL is found at."
  (bind ((dynsym (dynsym elf-object))
         (dynsym-size (slot-value dynsym 'sh-size))
         (symbol-table-entry-size (sizeof 'symbol-table-entry)))
         ;; bindings
    (do* ((i 0 (1+ i))
          (size 0 (+ symbol-table-entry-size size)))
         ;; end condition
         ((= i (/ dynsym-size symbol-table-entry-size)))
      (file-position stream (+ size (slot-value dynsym 'sh-offset)))
      (bind ((symbol-entry (read-binary 'symbol-table-entry stream))
             (offset       (slot-value symbol-entry 'st-name))
             (current-str  (read-string-table-string elf-object stream offset)))
        (when (string= search-symbol current-str)
          (return i))))))

(defun read-rela-plt-section (elf-object stream)
  "read the .rela.plt section."
  (find-in-section-table elf-object stream ".rela.plt"))

(defun find-plt-entry-by-index (elf-object stream index)
  "Find the rela entry associated at the position specified by INDEX."
  (bind ((rela-plt-section (rela-plt elf-object))
         (rela-plt-size    (slot-value rela-plt-section 'sh-size))
         (rela-size        (sizeof 'rela)))
         ;; bindings
    (do* ((i 0 (1+ i))
          (size 0 (+ rela-size size)))
         ;; end condition
         ((= i (/ rela-plt-size rela-size)))
      (file-position stream (+ size (slot-value rela-plt-section 'sh-offset)))
      (bind ((rela-entry    (read-binary 'rela stream))
             ;; Need to perform a shift due to where the index
             ;; is located. See 'man elf'.
             (current-index (ash (slot-value rela-entry 'r-info) -32)))
        (when (= index current-index)
          (return rela-entry))))))

(defun initialize-elf-with-stream (stream)
  "Returns a new elf object with data obtained from STREAM.
Note that STREAM should be opened with :element-type '(unsigned-byte 8) or
WITH-BINARY-FILE."
  (bind ((elf (make-instance 'elf))
         ((:accessors header section-table dynsym rela-plt) elf))
    (setf header           (read-header stream)
          section-table    (read-section-header-table elf stream)
          dynsym           (read-dynsym-section elf stream)
          rela-plt         (read-rela-plt-section elf stream))
      elf))
