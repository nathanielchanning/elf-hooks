(in-package binary-elf)

(defclass elf ()
  ((file-name :accessor file-name)
   (header :accessor header
           :documentation "An elf header.")
   (section-table :accessor section-table
                  :documentation "The section header table.")
   (dynsym-section :accessor dynsym
                   :documentation "The section for the dynamic symbol table.")
   (rela-plt-section :accessor rela-plt)))

;;;; This file is partially copied from binary-type's example file.
;;;; The below license is required by the file.

;;;;######################################################################
;;;;##
;;;;##    Copyright (C) 1999,
;;;;##    Department of Computer Science, University of Tromsø, Norway
;;;;##
;;;;## Filename:      COPYING
;;;;## Description:   Defines the terms under which this software may be copied.
;;;;## Author:        Frode Vatvedt Fjeld <frodef@acm.org>
;;;;## Created at:    Mon Nov  8 20:32:12 1999
;;;;## Distribution:  See the accompanying file COPYING.
;;;;##
;;;;## $Id: COPYING,v 1.1.1.1 2004/01/13 11:13:13 ffjeld Exp $
;;;;##
;;;;######################################################################
;;;;
;;;;Redistribution and use in source and binary forms, with or without
;;;;modification, are permitted provided that the following conditions
;;;;are met:
;;;;  1. Redistributions of source code must retain the above copyright
;;;;     notice, this list of conditions and the following disclaimer.
;;;;  2. Redistributions in binary form must reproduce the above copyright
;;;;     notice, this list of conditions and the following disclaimer in the
;;;;     documentation and/or other materials provided with the distribution.
;;;;  3. Neither the name of the University nor the names of its contributors
;;;;     may be used to endorse or promote products derived from this software
;;;;     without specific prior written permission.

;;; ELF basic types
;;; These are 64 bit values.
(define-unsigned word  4)
(define-signed sword   4)
(define-unsigned xword 8)
(define-signed sxword  8)
(define-unsigned addr  8)
(define-unsigned off   8)
(define-unsigned half  2)

;;; ELF file header structure
(define-binary-class header ()
  ((e-ident ; Information about the file.
    :binary-type (define-binary-struct e-ident ()
		   (ei-magic nil :binary-type
			     (define-binary-struct ei-magic ()
			       (ei-mag0 0 :binary-type u8)
			       (ei-mag1 #\null :binary-type char8)
			       (ei-mag2 #\null :binary-type char8)
			       (ei-mag3 #\null :binary-type char8)))
		   (ei-class nil :binary-type
			     (define-enum ei-class (u8)
			       elf-class-none 0
			       elf-class-32   1
			       elf-class-64   2))
		   (ei-data nil :binary-type
			    (define-enum ei-data (u8)
			      elf-data-none 0
			      elf-data-2lsb 1
			      elf-data-2msb 2))
		   (ei-version 0 :binary-type u8)
		   (padding nil :binary-type 1)
		   (ei-name "" :binary-type
			    (define-null-terminated-string ei-name 8))))
   (e-type ; object file type.
    :binary-type (define-enum e-type (half)
		   et-none 0
		   et-rel  1
		   et-exec 2
		   et-dyn  3
		   et-core 4
		   et-loproc #xff00
		   et-hiproc #xffff))
   (e-machine ; target architecture.
    :binary-type (define-enum e-machine (half)
		   em-none  0
		   em-m32   1
		   em-sparc 2
		   em-386   3
		   em-68k   4
		   em-88k   5
		   em-860   7
		   em-mips  8))
   (e-version   :binary-type word)   ; version of the object file format.
   (e-entry     :binary-type addr)   ; virtual adress of program entry point. 0 if no entry.
   (e-phoff     :binary-type off)    ; offset of program header table.
   (e-shoff     :binary-type off)    ; offset of section header table.
   (e-flags     :binary-type word)   ; processor specific flags.
   (e-ehsize    :binary-type half)   ; size of the elf header.
   (e-phentsize :binary-type half)   ; size of program header table entry.
   (e-phnum     :binary-type half)   ; number of entries in program header table.
   (e-shentsize :binary-type half)   ; size of section header table entry.
   (e-shnum     :binary-type half)   ; number of entries in the section header table.
   (e-shstrndx  :binary-type half))) ; section header table index which the entry for the string table.

;;; ELF64_Shdr
(define-binary-class section-header ()
  ((sh-name ; offset to address of the section name, a c string.
    :binary-type word)
   (sh-type ; identifies the section type.
    :binary-type (define-enum sh-type (word)
                   sht-null     0
                   sht-progbits 1
                   sht-symtab   2
                   sht-strtab   3
                   sht-rela     4
                   sht-hash     5
                   sht-dynamic  6
                   sht-note     7
                   sht-nobits   8
                   sht-rel      9
                   sht-shlib    10
                   sht-dynsym   11
                   sht-loos     #x60000000
                   sht-hios     #x6fffffff
                   sht-loproc   #x70000000
                   sht-hiproc   #x7fffffff))
   (sh-flags ; identifies the attributes of the section.
    :binary-type (define-enum sh-flags (xword)
                   shf-write     #x1
                   shf-alloc     #x2
                   shf-execinstr #x4
                   shf-maskos    #x0f000000
                   shf-maskproc  #xf0000000))
   (sh-addr
    :binary-type addr)    ; address of the beginning of the section in memory.
   (sh-offset
    :binary-type off)     ; offset of the beginning of the section contents in the file.
   (sh-size
    :binary-type xword)   ; size of the section.
   (sh-link
    :binary-type word)    ; section index of the section.
   (sh-info
    :binary-type word)    ; extra information of the section.
   (sh-addralign
    :binary-type xword)   ; the required alignment for the section.
   (sh-entsize
    :binary-type xword))) ; contains the size of each entry, zero if not fixed-size.

(define-binary-class symbol-table-entry ()
  ((st-name  ; offset to symbol name in the symbol table.
    :binary-type word)
   (st-info  ; contains the symbol type and binding attributes.
             ; binding attributes are the upper 4 bits while the
             ; type is the lower 4.
    :binary-type u8)
   (st-other ; reserved. Should be 0.
    :binary-type u8)
   (st-shndx ; section index of the section where the symbol is defined.
    :binary-type (define-enum st-shndx (half)
                   shn-undef  #x0
                   shn-loproc #xff00
                   shn-hiproc #xff1f
                   shn-loos   #xff20
                   shn-hios   #xff3f
                   shn-abs    #xff1f
                   shn-common #xfff2))
   (st-value ; value of the symbol. Changes based on file type.
    :binary-type addr)
   (st-size  ; size (bytes) associated with the symbol. 0 is 0 or unknown.
    :binary-type xword)))

(define-binary-class rela ()
  ((r-offset ; location at which the relocation is applied.
    :binary-type addr)
   (r-info   ; a symbol table index and a relocation type. possibly use bitfield here.
    :binary-type xword)
   (r-addend ; a constant addend used to compute value stored in relocated field.
    :binary-type sxword)))

(define-binary-class program-header-table-entry ()
  ((p-type ; identifies type of segment.
    :binary-type (define-enum p-type (word)
                   pt-null    0
                   pt-load    1
                   pt-dynamic 2
                   pt-interp  3
                   pt-note    4
                   pt-shlib   5
                   pt-phdr    6
                   pt-loos    #x60000000
                   pt-hios    #x6fffffff
                   pt-loproc  #x70000000
                   pt-hiproc  #x7fffffff))
   (p-flags ; segment attributes. Top 8 bits for processor, lower for environment use.
    :binary-type (define-bitfield p-flags (word)
                   (((:bits)
                      pf-x #x1
                      pf-w #x2
                      pf-r #x4))))
   (p-offset ;offset of segment from beginning of file.
    :binary-type off)
   (p-vaddr  ; virtual address of segment in memory.
    :binary-type addr)
   (p-addr   ; reserved for systems with physical addressing.
    :binary-type addr)
   (p-filesz ; size of file image of the segment.
    :binary-type xword)
   (p-memsz  ; size of memory image of the segment.
    :binary-type xword)
   (p-align  ; alignment constraint of the segment. power of 2.
    :binary-type xword)))

(define-binary-class dynamic-table-entry ()
  ((d-tag ; type of dynamic table entry.
    :binary-type (define-enum d-tag (sxword)
                   ;; values are from linux/elf.h
                   dt-null       0
                   dt-needed     1
                   dt-pltrelsz   2
                   dt-pltgot     3
                   dt-hash       4
                   dt-strtab     5
                   dt-symtab     6
                   dt-rela       7
                   dt-relasz     8
                   dt-relaent    9
                   dt-strsz      10
                   dt-syment     11
                   dt-init       12
                   dt-fini       13
                   dt-soname     14
                   dt-rpath      15
                   dt-symbolic   16
                   dt-rel        17
                   dt-relsz      18
                   dt-relent     19
                   dt-pltrel     20
                   dt-debug      21
                   dt-textrel    22
                   dt-jmprel     23
                   dt-encoding   32
                   old-dt-loos   #x60000000
                   dt-loos       #x6000000d
                   dt-hios       #x6ffff000
                   dt-valrnglo   #x6ffffd00
                   dt-valrnghi   #x6ffffdff
                   dt-addrrnglo  #x6ffffe00
                   dt-addrrnghi  #x6ffffeff
                   dt-versym     #x6ffffff0
                   dt-relacount  #x6ffffff9
                   dt-relcount   #x6ffffffa
                   dt-flags-1    #x6ffffffb
                   dt-verdef     #x6ffffffc
                   dt-verdefnum  #x6ffffffd
                   dt-verneed    #x6ffffffe
                   dt-verneednum #x6fffffff
                   dt-loproc     #x70000000
                   dt-hiproc     #x7fffffff))
   (d-val ; or d-ptr
          ; either a value or pointer.
    :binary-type xword)))
