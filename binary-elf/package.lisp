(defpackage binary-elf
  (:use cl binary-types bind)
  (:export
           ;; definitions
           elf
           file-name
           header
           section-table
           dynsym-section
           dynsym
           rela-plt-section
           rela-plt
           symbol-table-entry
           r-offset
           sh-offset
           st-value

           ;; functions
           initialize-elf-with-stream
           find-in-dynsym
           find-plt-entry-by-index))
