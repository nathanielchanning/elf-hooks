# ELF-Hooks

This package provides a way to hook functions in
shared objects that are in the ELF file format. The package
has only been tested on x64 Linux. Any changes
made by the functions in this package will reflect
across the whole process, so pay close attention
to what libraries are being used by your Common Lisp
distribution.
